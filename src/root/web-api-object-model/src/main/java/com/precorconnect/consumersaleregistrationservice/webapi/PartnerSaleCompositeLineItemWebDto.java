package com.precorconnect.consumersaleregistrationservice.webapi;

import java.util.Collection;

public class PartnerSaleCompositeLineItemWebDto {
	
	private Collection<PartnerSaleSimpleLineItemWebDto> components;
	
	public Collection<PartnerSaleSimpleLineItemWebDto> getComponents() {
		return components;
	}

	public void setComponents(Collection<PartnerSaleSimpleLineItemWebDto> components) {
		this.components = components;
	}

	@Override
	public String toString() {
		return "PartnerSaleCompositeLineItemWebDto [components=" + components
				+ "]";
	}

}
