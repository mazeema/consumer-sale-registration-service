package com.precorconnect.consumersaleregistrationservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;

public interface ConsumerSaleRegSynopsisViewFactory {


	 ConsumerSaleRegSynopsisView construct(
	            @NonNull ConsumerPartnerSaleRegDraftView consumerCommercialSaleRegDraftView
	    );
}
