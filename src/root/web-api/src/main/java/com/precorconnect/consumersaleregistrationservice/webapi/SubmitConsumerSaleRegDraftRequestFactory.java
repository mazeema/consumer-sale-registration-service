package com.precorconnect.consumersaleregistrationservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.SubmitConsumerSaleRegDraftRequestDto;

public interface SubmitConsumerSaleRegDraftRequestFactory {

	SubmitConsumerSaleRegDraftRequestDto construct(
			@NonNull SubmitConsumerSaleRegDraftDto submitConsumerSaleRegDraftDto
			);
}
	