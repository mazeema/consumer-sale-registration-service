package com.precorconnect.consumersaleregistrationservice.webapi;

import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegistrationService;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegistrationServiceConfigFactoryImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegistrationServiceImpl;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.inject.Singleton;

@Configuration
class ConsumerSaleRegistrationServiceBean {

    @Bean
    @Singleton
    public ConsumerSaleRegistrationService partnerSaleRegistrationService(
    ) {

        return
                new ConsumerSaleRegistrationServiceImpl(
                        new ConsumerSaleRegistrationServiceConfigFactoryImpl()
                                .construct()
                );

    }

}
