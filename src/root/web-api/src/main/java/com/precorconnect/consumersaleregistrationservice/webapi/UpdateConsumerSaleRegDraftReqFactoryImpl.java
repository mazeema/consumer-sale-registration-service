package com.precorconnect.consumersaleregistrationservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.FirstName;
import com.precorconnect.FirstNameImpl;
import com.precorconnect.LastName;
import com.precorconnect.LastNameImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerPhoneNumber;
import com.precorconnect.consumersaleregistrationservice.ConsumerPhoneNumberImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerPostalAddress;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItem;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftIdImpl;
import com.precorconnect.consumersaleregistrationservice.CreateDate;
import com.precorconnect.consumersaleregistrationservice.CreateDateImpl;
import com.precorconnect.consumersaleregistrationservice.InvoiceNumber;
import com.precorconnect.consumersaleregistrationservice.InvoiceNumberImpl;
import com.precorconnect.consumersaleregistrationservice.InvoiceUrl;
import com.precorconnect.consumersaleregistrationservice.InvoiceUrlImpl;
import com.precorconnect.consumersaleregistrationservice.IsSubmit;
import com.precorconnect.consumersaleregistrationservice.IsSubmitImpl;
import com.precorconnect.consumersaleregistrationservice.PersonEmail;
import com.precorconnect.consumersaleregistrationservice.PersonEmailImpl;
import com.precorconnect.consumersaleregistrationservice.SellDate;
import com.precorconnect.consumersaleregistrationservice.SellDateImpl;
import com.precorconnect.consumersaleregistrationservice.UpdateConsumerSaleRegDraftReq;


@Singleton
@Component
public class UpdateConsumerSaleRegDraftReqFactoryImpl
		implements UpdateConsumerSaleRegDraftReqFactory {
	
	 private final PostalAddressFactory postalAddressFactory;

	private final ConsumerSaleRegDraftSaleSimpleLineItemFactory consumerSaleRegDraftSaleSimpleLineItemFactory;

	private final ConsumerSaleRegDraftSaleCompositeLineItemFactory partnerSaleRegDraftSaleCompositeLineItemFactory;



	@Inject
	public UpdateConsumerSaleRegDraftReqFactoryImpl(
			@NonNull PostalAddressFactory postalAddressFactory,
			@NonNull ConsumerSaleRegDraftSaleSimpleLineItemFactory consumerSaleRegDraftSaleSimpleLineItemFactory,
			@NonNull ConsumerSaleRegDraftSaleCompositeLineItemFactory partnerSaleRegDraftSaleCompositeLineItemFactory) {
	
		this.postalAddressFactory = 
				guardThat(
						"postalAddressFactory",
						postalAddressFactory
				)
						.isNotNull()
						.thenGetValue();
		
		this.consumerSaleRegDraftSaleSimpleLineItemFactory = 
				guardThat(
						"consumerSaleRegDraftSaleSimpleLineItemFactory",
						consumerSaleRegDraftSaleSimpleLineItemFactory
				)
						.isNotNull()
						.thenGetValue();
		
		this.partnerSaleRegDraftSaleCompositeLineItemFactory = 
				guardThat(
						"partnerSaleRegDraftSaleCompositeLineItemFactory",
						partnerSaleRegDraftSaleCompositeLineItemFactory
				)
						.isNotNull()
						.thenGetValue();
		
}




	@Override
	public UpdateConsumerSaleRegDraftReq construct(
			 com.precorconnect.consumersaleregistrationservice.webapi. @NonNull UpdateConsumerSaleRegDraftReq updatePartnerCommercialSaleRegDraftReq
			 ) {
		try {

			ConsumerSaleRegDraftId id=
					new ConsumerSaleRegDraftIdImpl(updatePartnerCommercialSaleRegDraftReq.getId());
			
			 FirstName firstName =
		                new FirstNameImpl(
		                		updatePartnerCommercialSaleRegDraftReq
		                		.getFirstName()
		                );
			 
			 LastName lastName =
		                new LastNameImpl(
		                		updatePartnerCommercialSaleRegDraftReq
		                		.getLastName()
		                );

			 ConsumerPostalAddress address =
		                postalAddressFactory
		                        .construct(
		                        		updatePartnerCommercialSaleRegDraftReq
		                        		.getAddress()
		                        		);

		        ConsumerPhoneNumber phoneNumber =
		                		updatePartnerCommercialSaleRegDraftReq
		                		.getPhoneNumber()
		                		.map(ConsumerPhoneNumberImpl::new)
		    				 	.orElse(null);
		        
		        PersonEmail emailAddress=
		        				updatePartnerCommercialSaleRegDraftReq
		        				.getPersonEmail()
		        				.map(PersonEmailImpl::new)
		    				 	.orElse(null);
			 
			

			 AccountId partnerAccountId =
					 new AccountIdImpl(
							 updatePartnerCommercialSaleRegDraftReq
							 				.getPartnerAccountId()
							 );
		  

		     InvoiceNumber invoiceNum =
		    		 new InvoiceNumberImpl(
		    				 updatePartnerCommercialSaleRegDraftReq
		    				 	.getInvoiceNumber()
		    				 );

		     InvoiceUrl invoiceUrl =
		    				 updatePartnerCommercialSaleRegDraftReq
		    				 	.getInvoiceUrl()
		    				 	.map(InvoiceUrlImpl::new)
		    				 	.orElse(null);

		     IsSubmit isSubmitted =
		    		 new IsSubmitImpl(
		    				 updatePartnerCommercialSaleRegDraftReq
		    				 	.isSubmitted()
		    				 );

		       SellDate	sellDate = new SellDateImpl(
						 new SimpleDateFormat("MM/dd/yyyy").parse(
								 updatePartnerCommercialSaleRegDraftReq
								 .getSellDate()
						 	).toInstant()
						 );
			

		     UserId userId =
		    				 updatePartnerCommercialSaleRegDraftReq
		    				 	.getPartnerRepUserId()
		    				 	.map(UserIdImpl::new)
		    				 	.orElse(null);


		     Collection<ConsumerSaleLineItem> saleLineItems = new ArrayList<>();

			     saleLineItems.addAll(
				     StreamSupport
			         .stream(
			        		 updatePartnerCommercialSaleRegDraftReq.getSimpleLineItems()
			                         .spliterator(),
			                 false)			         
			         .map(consumerSaleRegDraftSaleSimpleLineItemFactory::construct)
			         .collect(Collectors.toList())
			     );
			     saleLineItems.addAll(
				     StreamSupport
			         .stream(
			        		 updatePartnerCommercialSaleRegDraftReq.getCompositeLineItems()
			                         .spliterator(),
			                 false)
			         .map(partnerSaleRegDraftSaleCompositeLineItemFactory::construct)
			         .collect(Collectors.toList())
		    	);
			     
			     CreateDate  createDate=
			    		 new CreateDateImpl(
								 new SimpleDateFormat("MM/dd/yyyy").parse(
										 updatePartnerCommercialSaleRegDraftReq
										 .getCreateDate()
								 	).toInstant()
								 );

		     return
		    		 new com.precorconnect.consumersaleregistrationservice.UpdateConsumerSaleRegDraftReqImpl(
		    				 id,
		    				 firstName,
		    				 lastName,
		    				 address,
		    				 phoneNumber,
		    				 emailAddress,
		    				 partnerAccountId,		    				 
		    				 invoiceNum,
		    				 invoiceUrl,
		    				 isSubmitted,
		    				 sellDate,
		    				 userId,
		    				 saleLineItems,
		    				 createDate
		    				 );

			 } catch (java.text.ParseException e)  {
					e.printStackTrace();
					return null;
				}
	}

}
