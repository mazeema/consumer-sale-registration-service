package com.precorconnect.consumersaleregistrationservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.consumersaleregistrationservice.Amount;
import com.precorconnect.consumersaleregistrationservice.AmountImpl;
import com.precorconnect.consumersaleregistrationservice.Brand;
import com.precorconnect.consumersaleregistrationservice.BrandImpl;
import com.precorconnect.consumersaleregistrationservice.InstallDate;
import com.precorconnect.consumersaleregistrationservice.InstallDateImpl;
import com.precorconnect.consumersaleregistrationservice.PartnerCommericalSaleDraftId;
import com.precorconnect.consumersaleregistrationservice.PartnerCommericalSaleDraftIdImpl;
import com.precorconnect.consumersaleregistrationservice.PartnerCompositeSaleLineItemDto;
import com.precorconnect.consumersaleregistrationservice.PartnerSaleLineItemDto;
import com.precorconnect.consumersaleregistrationservice.ProductSaleRegistrationRuleEngineRequestDto;
import com.precorconnect.consumersaleregistrationservice.ProductSaleRegistrationRuleEngineRequestDtoImpl;
import com.precorconnect.consumersaleregistrationservice.SellDate;
import com.precorconnect.consumersaleregistrationservice.SellDateImpl;
import com.precorconnect.consumersaleregistrationservice.SubmittedDate;
import com.precorconnect.consumersaleregistrationservice.SubmittedDateImpl;



@Component
public class SpiffRuleEngineWebRequestFactoryImpl  
		implements SpiffRuleEngineWebRequestFactory {
	
	private  final PartnerSaleSimpleLineItemFactory partnerSaleSimpleLineItemFactory;
	
	private final PartnerSaleCompositeLineItemFactory partnerSaleCompositeLineItemFactory;
	
	
	@Inject
	public SpiffRuleEngineWebRequestFactoryImpl(
			@NonNull final PartnerSaleSimpleLineItemFactory partnerSaleSimpleLineItemFactory,
			@NonNull final PartnerSaleCompositeLineItemFactory partnerSaleCompositeLineItemFactory) {
		 
			this.partnerSaleSimpleLineItemFactory =
	                guardThat(
	                        "partnerSaleSimpleLineItemFactory",
	                        partnerSaleSimpleLineItemFactory
	                )
	                        .isNotNull()
	                        .thenGetValue();
			
			this.partnerSaleCompositeLineItemFactory =
	                guardThat(
	                        "partnerSaleCompositeLineItemFactory",
	                        partnerSaleCompositeLineItemFactory
	                )
	                        .isNotNull()
	                        .thenGetValue();
	}
	
	@Override
	public ProductSaleRegistrationRuleEngineRequestDto construct(
			ProductSaleRegistrationRuleEngineWebDto productSaleRegistrationRuleEngineWebDto
			) {
		
		try{
			
		PartnerCommericalSaleDraftId partnerSaleRegId = new PartnerCommericalSaleDraftIdImpl(
						productSaleRegistrationRuleEngineWebDto.getRegistrationId().isPresent()?productSaleRegistrationRuleEngineWebDto.getRegistrationId().get():null
						);
		
		SellDate sellDate = new SellDateImpl(
				 new SimpleDateFormat("MM/dd/yyyy").parse(
						 productSaleRegistrationRuleEngineWebDto
						 .getSellDate()
				 	).toInstant()
				);
		
		InstallDate installDate = new InstallDateImpl( 
				new SimpleDateFormat("MM/dd/yyyy").parse(
				productSaleRegistrationRuleEngineWebDto
				 .getInstallDate().isPresent()
				 ?productSaleRegistrationRuleEngineWebDto
				 .getInstallDate().get():null	 
				).toInstant());
		
		long actualEpochMilli =
	    		  new Date().toInstant().toEpochMilli();

	        long jdbcConversionCompensatedEpochMilli =
	                actualEpochMilli - Calendar.getInstance().getTimeZone().getRawOffset();

	        String  draftedDate = 
	        		new SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
	        			.format(
	        					new Timestamp(
	        					jdbcConversionCompensatedEpochMilli
	        					)
	        				);
			
		SubmittedDate submittedDate = new SubmittedDateImpl(draftedDate);
		
		Brand brand = new BrandImpl(productSaleRegistrationRuleEngineWebDto.getFacilityBrand());
		
		Amount spiffAmount = new AmountImpl(0.0d);
		
		List<PartnerSaleLineItemDto> simpleLineItems= new ArrayList<>();
		
		if(productSaleRegistrationRuleEngineWebDto.getSimpleLineItems().isPresent()) {
		simpleLineItems.addAll(
			     StreamSupport
		         .stream(
		        		 productSaleRegistrationRuleEngineWebDto.getSimpleLineItems().get()
		                         .spliterator(),
		                 false)
		         .map(partnerSaleSimpleLineItemFactory::construct)
		         .collect(Collectors.toList()));
		}
		List<PartnerCompositeSaleLineItemDto> compositeLineItems = new ArrayList<>();
		if(productSaleRegistrationRuleEngineWebDto.getCompositeLineItems().isPresent()) {
		compositeLineItems.addAll(
			     StreamSupport
		         .stream(
		        		 productSaleRegistrationRuleEngineWebDto.getCompositeLineItems().get()
		                         .spliterator(),
		                 false)
		         .map(partnerSaleCompositeLineItemFactory::construct)
		         .collect(Collectors.toList()));
		}
		
		
		return new ProductSaleRegistrationRuleEngineRequestDtoImpl(
				partnerSaleRegId, 
				sellDate, 
				installDate, 
				submittedDate, 
				brand,
				simpleLineItems,
				compositeLineItems,
				spiffAmount);
	
		} catch (java.text.ParseException e)  {
			e.printStackTrace();
			return null;
		}

	}

}
