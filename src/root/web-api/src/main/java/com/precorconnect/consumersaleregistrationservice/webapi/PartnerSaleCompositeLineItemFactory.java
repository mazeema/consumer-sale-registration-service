package com.precorconnect.consumersaleregistrationservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.PartnerCompositeSaleLineItemDto;


public interface PartnerSaleCompositeLineItemFactory {
	
	PartnerCompositeSaleLineItemDto construct(
			@NonNull PartnerSaleCompositeLineItemWebDto partnerSaleLineSimpleItemWebDto
			);

}
