package com.precorconnect.consumersaleregistrationservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.PartnerSaleLineItemDto;


public interface PartnerSaleSimpleLineItemFactory {
	
	PartnerSaleLineItemDto construct(
			@NonNull PartnerSaleSimpleLineItemWebDto partnerSaleLineSimpleItemWebDto
			);

}
