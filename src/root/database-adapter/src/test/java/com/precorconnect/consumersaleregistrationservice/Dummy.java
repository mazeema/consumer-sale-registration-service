package com.precorconnect.consumersaleregistrationservice;

import java.net.URI;
import java.net.URISyntaxException;

import com.precorconnect.Password;
import com.precorconnect.PasswordImpl;
import com.precorconnect.Username;
import com.precorconnect.UsernameImpl;

public class Dummy {

    /*
    fields
     */
    private final URI uri;

    private final Username username =
            new UsernameImpl("username");

    private final Password password =
            new PasswordImpl("password");

    /*
    constructors
     */
    {
        try {

            uri = new URI("http://dev.precorconnect.com");

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }
    }

    /*
    getter methods
     */

    public URI getUri() {
        return uri;
    }

    public Username getUsername() {
        return username;
    }

    public Password getPassword() {
        return password;
    }
    
    
    
}
