package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.database.DatabaseAdapterConfig;

public class Config {

    /*
    fields
     */
    private final DatabaseAdapterConfig databaseAdapterConfig;

    /*
    constructors
     */
    public Config(
    	@NonNull final DatabaseAdapterConfig databaseAdapterConfig
    		) {
        
    	this.databaseAdapterConfig =
                guardThat("databaseAdapterConfig",
                		databaseAdapterConfig
                )
                        .isNotNull()
                        .thenGetValue();
                   
    }

    /*
    getter methods
    */
    public DatabaseAdapterConfig getDatabaseAdapterConfig() {

        return databaseAdapterConfig;

    }
}
