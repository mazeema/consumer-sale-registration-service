CREATE TABLE ConsumerSaleRegDrafts (
  Id                     BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  PartnerAccountId       CHAR(18)                          NOT NULL,
  SaleInvoiceId          VARCHAR(43),
  SaleCreatedAtTimestamp DATETIME,
  SalePartnerRepId       VARCHAR(1000)
);

CREATE TABLE ConsumerPartnerSaleRegDrafts (
  Id                     BIGINT PRIMARY KEY NOT NULL,
  FirstName			     VARCHAR(300) NOT NULL,
  LastName				 VARCHAR(300) NOT NULL,
  AddressLine1			 VARCHAR(300) NOT NULL,
  AddressLine2			 VARCHAR(300),
  City					 VARCHAR(300) NOT NULL,
  Region				 VARCHAR(300) NOT NULL,
  PostalCode			 VARCHAR(300) NOT NULL,
  Country				 VARCHAR(300) NOT NULL,
  Phone 				 VARCHAR(300),
  Email					 VARCHAR(300),
  InvoiceNumber          VARCHAR(100) NOT NULL,
  InvoiceUrl  VARCHAR(300),
  IsSubmitted BOOLEAN DEFAULT FALSE,
  SellDate DATE NOT NULL,
  CreatedDate DATETIME default CURRENT_TIMESTAMP,
  FOREIGN KEY (Id) REFERENCES ConsumerSaleRegDrafts (Id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE ConsumerSaleRegDraftSaleLineItems (
  Id                     BIGINT   PRIMARY KEY AUTO_INCREMENT NOT NULL,
  ConsumerSaleRegDraftId BIGINT                            NOT NULL,
  FOREIGN KEY (ConsumerSaleRegDraftId) REFERENCES ConsumerSaleRegDrafts (Id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE ConsumerSaleRegDraftSaleSimpleLineItems (
  Id      BIGINT PRIMARY KEY NOT NULL,
  AssetId CHAR(40)           NOT NULL,
  SerialNumber CHAR(30) NOT NULL,
  ProductLineId BIGINT NOT NULL,
  ProductLineName CHAR(100) NOT NULL,
  ProductGroupId BIGINT,
  ProductGroupName CHAR(100),
  Price   NUMERIC(13, 4),
  FOREIGN KEY (Id) REFERENCES ConsumerSaleRegDraftSaleLineItems (Id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE ConsumerSaleRegDraftSaleCompositeLineItems (
  Id    BIGINT PRIMARY KEY NOT NULL,
  Price NUMERIC(13, 4),
  FOREIGN KEY (Id) REFERENCES ConsumerSaleRegDraftSaleLineItems (Id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE ConsumerSaleLineItemComponents (
  Id                                         BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  AssetId                                    CHAR(40)                          NOT NULL,
  SerialNumber CHAR(30) NOT NULL,
  ProductLineId BIGINT NOT NULL,
  ProductLineName CHAR(100) NOT NULL,
  ProductGroupId BIGINT,
  ProductGroupName CHAR(100),
  Price                                      NUMERIC(13, 4),
  ConsumerSaleRegDraftSaleCompositeLineItemId BIGINT                            NOT NULL,
  FOREIGN KEY (ConsumerSaleRegDraftSaleCompositeLineItemId) REFERENCES ConsumerSaleRegDraftSaleCompositeLineItems (Id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)

