package com.precorconnect.consumersaleregistrationservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

interface ConsumerSaleLineItemComponentFactory {

    ConsumerSaleLineItemComponent construct(
            com.precorconnect.consumersaleregistrationservice.
            @NonNull ConsumerSaleLineItemComponent consumerSaleLineItemComponent
    );

    com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItemComponent construct(
            @NonNull ConsumerSaleLineItemComponent consumerSaleLineItemComponent
    );

}
