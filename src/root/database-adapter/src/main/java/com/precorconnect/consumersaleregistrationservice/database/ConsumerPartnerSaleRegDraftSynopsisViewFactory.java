package com.precorconnect.consumersaleregistrationservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftSynopsisView;

interface ConsumerPartnerSaleRegDraftSynopsisViewFactory {

    com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftSynopsisView construct(
            @NonNull ConsumerPartnerSaleRegDraftSynopsisView consumerCommercialSaleRegDraftSynopsisView
    );

}
