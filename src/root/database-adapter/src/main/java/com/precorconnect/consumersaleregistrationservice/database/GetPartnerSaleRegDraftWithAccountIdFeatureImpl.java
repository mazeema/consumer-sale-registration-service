package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.precorconnect.AccountId;
import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;

@Singleton
public class GetPartnerSaleRegDraftWithAccountIdFeatureImpl implements
		GetPartnerSaleRegDraftWithAccountIdFeature {

    /*
    fields
     */
    private final SessionFactory sessionFactory;

    private final ConsumerSaleRegDraftViewFactory consumerCommercialSaleRegDraftViewFactory;

    /*
    constructors
     */
    @Inject
    public GetPartnerSaleRegDraftWithAccountIdFeatureImpl(
            @NonNull SessionFactory sessionFactory,
            @NonNull ConsumerSaleRegDraftViewFactory partnerCommercialSaleRegDraftViewFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.consumerCommercialSaleRegDraftViewFactory =
                guardThat(
                        "partnerCommercialSaleRegDraftViewFactory",
                         partnerCommercialSaleRegDraftViewFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }


	@SuppressWarnings("unchecked")
	@Override
	public Collection<ConsumerPartnerSaleRegDraftView> execute(
			@NonNull AccountId accountId) {

        Session session = null;

        try {

            session = sessionFactory.openSession();

            Query query = session.createQuery("from ConsumerPartnerSaleRegDraft where partnerAccountId = :accountId and isSubmitted = :submitStatus");

            query.setString("accountId", accountId.getValue());

            query.setBoolean("submitStatus", false);

            Collection<ConsumerPartnerSaleRegDraft> consumerPartnerSaleRegDrafts =
            																	query.list();

            return
            		consumerPartnerSaleRegDrafts
            		.stream()
            		.map(consumerCommercialSaleRegDraftViewFactory :: construct)
            		.collect(Collectors.toList());

        } finally {

            if (null != session) {
                session.close();
            }

        }

    }

}
