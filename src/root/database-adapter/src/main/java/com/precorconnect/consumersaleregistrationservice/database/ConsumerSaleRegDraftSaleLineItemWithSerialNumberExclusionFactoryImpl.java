package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItem;
import com.precorconnect.consumersaleregistrationservice.SerialNumber;
import com.precorconnect.consumersaleregistrationservice.SerialNumberImpl;

@Singleton
public class ConsumerSaleRegDraftSaleLineItemWithSerialNumberExclusionFactoryImpl implements 
												ConsumerSaleRegDraftSaleLineItemWithSerialNumberExclusionFactory {
	

    /*
    fields
     */
    private final ConsumerSaleRegDraftSaleSimpleLineItemFactory consumerSaleRegDraftSaleSimpleLineItemFactory;

    private final ConsumerSaleRegDraftSaleCompositeLineItemFactory consumerSaleRegDraftSaleCompositeLineItemFactory;

    /*
    constructors
     */
    @Inject
    public ConsumerSaleRegDraftSaleLineItemWithSerialNumberExclusionFactoryImpl(
            @NonNull ConsumerSaleRegDraftSaleSimpleLineItemFactory partnerSaleRegDraftSaleSimpleLineItemFactory,
            @NonNull ConsumerSaleRegDraftSaleCompositeLineItemFactory partnerSaleRegDraftSaleCompositeLineItemFactory
    ) {

    	this.consumerSaleRegDraftSaleSimpleLineItemFactory =
                guardThat(
                        "partnerSaleRegDraftSaleSimpleLineItemFactory",
                         partnerSaleRegDraftSaleSimpleLineItemFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.consumerSaleRegDraftSaleCompositeLineItemFactory =
                guardThat(
                        "partnerSaleRegDraftSaleCompositeLineItemFactory",
                         partnerSaleRegDraftSaleCompositeLineItemFactory
                )
                        .isNotNull()
                        .thenGetValue();
    }

	@Override
	public ConsumerSaleRegDraftSaleLineItem construct(
			com.precorconnect.consumersaleregistrationservice.database.@NonNull ConsumerSaleRegDraftSaleLineItem consumerSaleRegDraftSaleLineItem,
			@NonNull Collection<SerialNumber> serialNumberExclusionList) {
		
		
		if (consumerSaleRegDraftSaleLineItem instanceof ConsumerSaleRegDraftSaleSimpleLineItem) {
			
			SerialNumber serialNumber=
					new SerialNumberImpl(((ConsumerSaleRegDraftSaleSimpleLineItem) consumerSaleRegDraftSaleLineItem).getSerialNumber());
			
			if(!serialNumberExclusionList.contains(serialNumber)){
				
				return
	                    consumerSaleRegDraftSaleSimpleLineItemFactory
	                            .construct(
	                                    (ConsumerSaleRegDraftSaleSimpleLineItem) consumerSaleRegDraftSaleLineItem
	                            );
			}else
			{
				return 
						null;
			}

            

        } else if (consumerSaleRegDraftSaleLineItem instanceof ConsumerSaleRegDraftSaleCompositeLineItem) {
        	
        	 boolean flag=false;
    		 
    		 Collection<com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleLineItemComponent> consumerSaleLineItemComponentList = 
	    										(Collection<com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleLineItemComponent>) 
	    										((ConsumerSaleRegDraftSaleCompositeLineItem)consumerSaleRegDraftSaleLineItem)
	    										.getComponents();
	    	
	    	for(com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleLineItemComponent consumerSaleLineItemComponent:consumerSaleLineItemComponentList){
	    		
	    		SerialNumber serialNumber = 
	    				new SerialNumberImpl(
	    						consumerSaleLineItemComponent
	    							.getSerialNumber()
	    							);
	    		
	    		if(serialNumberExclusionList.contains(serialNumber)){
	    			flag=true;
	        	}
	    		
	    	}
	    	
	    	if(!flag){
            return
                    consumerSaleRegDraftSaleCompositeLineItemFactory
                            .construct(
                                    (ConsumerSaleRegDraftSaleCompositeLineItem) consumerSaleRegDraftSaleLineItem
                            );
	    	}
	    	else{
	    		return null;
	    	}

        }
        // handle unexpected implementations
        throw
                new RuntimeException(
                        String.format(
                                "received unsupported %s implementation: %s",
                                ConsumerSaleRegDraftSaleLineItem.class,
                                consumerSaleRegDraftSaleLineItem.getClass()
                        )
                );

    }

}
