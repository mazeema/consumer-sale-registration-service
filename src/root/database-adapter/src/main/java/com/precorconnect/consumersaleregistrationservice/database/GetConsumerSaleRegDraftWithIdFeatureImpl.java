package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;

@Singleton
class GetConsumerSaleRegDraftWithIdFeatureImpl
        implements GetConsumerSaleRegDraftWithIdFeature {

    /*
    fields
     */
    private final SessionFactory sessionFactory;

    private final ConsumerSaleRegDraftViewFactory consumerSaleRegDraftViewFactory;

    /*
    constructors
     */
    @Inject
    public GetConsumerSaleRegDraftWithIdFeatureImpl(
            @NonNull SessionFactory sessionFactory,
            @NonNull ConsumerSaleRegDraftViewFactory consumerSaleRegDraftViewFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.consumerSaleRegDraftViewFactory =
                guardThat(
                        "consumerSaleRegDraftViewFactory",
                        consumerSaleRegDraftViewFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public ConsumerPartnerSaleRegDraftView execute(
            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId
    ) {

        Session session = null;

        try {

            session = sessionFactory.openSession();

            ConsumerPartnerSaleRegDraft consumerPartnerSaleRegDraft =
                    (ConsumerPartnerSaleRegDraft) session.get(
                    		ConsumerPartnerSaleRegDraft.class,
                            consumerSaleRegDraftId.getValue()
                    );

            return
                    consumerSaleRegDraftViewFactory
                            .construct(consumerPartnerSaleRegDraft);

        } finally {

            if (null != session) {
                session.close();
            }

        }

    }
}
