package com.precorconnect.consumersaleregistrationservice.database;

import java.math.BigDecimal;
import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleLineItemComponent;
import com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleRegDraftSaleCompositeLineItem;

@Entity
@Table(name = "ConsumerSaleLineItemComponents")
class ConsumerSaleLineItemComponent {

    /*
    fields
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String assetId;

    private BigDecimal price;

    private String serialNumber ;

    private Integer productLineId ;

    private String productLineName ;

    private Integer productGroupId ;

    private String productGroupName ;


    @ManyToOne
    @JoinColumn(
            name = "ConsumerSaleRegDraftSaleCompositeLineItemId",
            referencedColumnName = "Id"
    )
    private ConsumerSaleRegDraftSaleCompositeLineItem consumerSaleRegDraftSaleCompositeLineItem;

    /*
    getter & setter methods
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public Optional<BigDecimal> getPrice() {
        return Optional.ofNullable(price);
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public ConsumerSaleRegDraftSaleCompositeLineItem getConsumerSaleRegDraftSaleCompositeLineItem() {
		return consumerSaleRegDraftSaleCompositeLineItem;
	}

	public void setConsumerSaleRegDraftSaleCompositeLineItem(
			ConsumerSaleRegDraftSaleCompositeLineItem consumerSaleRegDraftSaleCompositeLineItem) {
		this.consumerSaleRegDraftSaleCompositeLineItem = consumerSaleRegDraftSaleCompositeLineItem;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Integer getProductLineId() {
		return productLineId;
	}

	public void setProductLineId(Integer productLineId) {
		this.productLineId = productLineId;
	}

	public String getProductLineName() {
		return productLineName;
	}

	public void setProductLineName(String productLineName) {
		this.productLineName = productLineName;
	}

	public Integer getProductGroupId() {
		return productGroupId;
	}

	public void setProductGroupId(Integer productGroupId) {
		this.productGroupId = productGroupId;
	}

	public String getProductGroupName() {
		return productGroupName;
	}

	public void setProductGroupName(String productGroupName) {
		this.productGroupName = productGroupName;
	}
	/*
    equality methods
    */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((assetId == null) ? 0 : assetId.hashCode());
		result = prime * result + ((consumerSaleRegDraftSaleCompositeLineItem == null) ? 0
				: consumerSaleRegDraftSaleCompositeLineItem.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((productGroupId == null) ? 0 : productGroupId.hashCode());
		result = prime * result + ((productGroupName == null) ? 0 : productGroupName.hashCode());
		result = prime * result + ((productLineId == null) ? 0 : productLineId.hashCode());
		result = prime * result + ((productLineName == null) ? 0 : productLineName.hashCode());
		result = prime * result + ((serialNumber == null) ? 0 : serialNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConsumerSaleLineItemComponent other = (ConsumerSaleLineItemComponent) obj;
		if (assetId == null) {
			if (other.assetId != null)
				return false;
		} else if (!assetId.equals(other.assetId))
			return false;
		if (consumerSaleRegDraftSaleCompositeLineItem == null) {
			if (other.consumerSaleRegDraftSaleCompositeLineItem != null)
				return false;
		} else if (!consumerSaleRegDraftSaleCompositeLineItem.equals(other.consumerSaleRegDraftSaleCompositeLineItem))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (productGroupId == null) {
			if (other.productGroupId != null)
				return false;
		} else if (!productGroupId.equals(other.productGroupId))
			return false;
		if (productGroupName == null) {
			if (other.productGroupName != null)
				return false;
		} else if (!productGroupName.equals(other.productGroupName))
			return false;
		if (productLineId == null) {
			if (other.productLineId != null)
				return false;
		} else if (!productLineId.equals(other.productLineId))
			return false;
		if (productLineName == null) {
			if (other.productLineName != null)
				return false;
		} else if (!productLineName.equals(other.productLineName))
			return false;
		if (serialNumber == null) {
			if (other.serialNumber != null)
				return false;
		} else if (!serialNumber.equals(other.serialNumber))
			return false;
		return true;
	}

}
