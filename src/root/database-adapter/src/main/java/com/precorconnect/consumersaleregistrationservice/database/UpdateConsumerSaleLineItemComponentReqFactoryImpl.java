package com.precorconnect.consumersaleregistrationservice.database;




import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;


@Singleton
public class UpdateConsumerSaleLineItemComponentReqFactoryImpl implements UpdateConsumerSaleLineItemComponentReqFactory {



	@Override
	public com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleLineItemComponent construct(
			 com.precorconnect.consumersaleregistrationservice.
			 @NonNull ConsumerSaleLineItemComponent partnerSaleLineItemComponent
			) {

		try {

		    com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleLineItemComponent objectUnderConstruction=
		    		new com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleLineItemComponent();


		    objectUnderConstruction
            .setId(
                    partnerSaleLineItemComponent
                            .getId()
                            .getValue()
            );

	        objectUnderConstruction
	                .setAssetId(
	                        partnerSaleLineItemComponent
	                                .getAssetId()
	                                .getValue()
	                );

	        objectUnderConstruction
		        .setSerialNumber(
		                partnerSaleLineItemComponent
		                        .getSerialNumber()
		                        .getValue()
		        );

	        objectUnderConstruction
		        .setProductGroupId(
		                partnerSaleLineItemComponent
		                        .getProductGroupId().isPresent()
			                    ?partnerSaleLineItemComponent.getProductGroupId().get().getValue()
					            :null
		        );

	        objectUnderConstruction
	        .setProductGroupName(
	        		partnerSaleLineItemComponent
		                    .getProductGroupName().isPresent()
		                    ?partnerSaleLineItemComponent.getProductGroupName().get().getValue()
		                    :null
	        );

	        objectUnderConstruction
		        .setProductLineId(
		                partnerSaleLineItemComponent
		                        .getProductLineId()
		                        .getValue()
		        );

	        objectUnderConstruction
		        .setProductLineName(
		                partnerSaleLineItemComponent
		                        .getProductLineName()
		                        .getValue()
		        );

	        objectUnderConstruction
	                .setPrice(
	                        partnerSaleLineItemComponent
	                                .getPrice()
	                                .orElse(null)
	                );

	           return objectUnderConstruction;

	        } catch(final Exception e) {

	        	throw new RuntimeException("exception while constructing PartnerSaleLineItemComponent:", e);

	        }
	}

}
