package com.precorconnect.consumersaleregistrationservice.database;

import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItemId;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
class RemoveSaleLineItemFromConsumerSaleRegDraftFeatureImpl
        implements RemoveSaleLineItemFromConsumerSaleRegDraftFeature {

    /*
    fields
     */
    private final SessionFactory sessionFactory;

    /*
    constructors
     */
    @Inject
    public RemoveSaleLineItemFromConsumerSaleRegDraftFeatureImpl(
            @NonNull SessionFactory sessionFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public void execute(
            @NonNull ConsumerSaleRegDraftSaleLineItemId partnerSaleRegDraftSaleLineItemId
    ) {

        Session session = null;

        try {

            session = sessionFactory.openSession();

            try {

                session.beginTransaction();

                ConsumerSaleRegDraftSaleLineItem consumerSaleRegDraftSaleLineItem =
                        (ConsumerSaleRegDraftSaleLineItem) session.get(
                        		ConsumerSaleRegDraftSaleLineItem.class,
                                partnerSaleRegDraftSaleLineItemId.getValue()
                        );

                session.delete(consumerSaleRegDraftSaleLineItem);

                session.getTransaction().commit();

            } catch (HibernateException e) {

                session.getTransaction().rollback();
                throw e;

            }

        } finally {

            if (null != session) {
                session.close();
            }

        }

    }
}
