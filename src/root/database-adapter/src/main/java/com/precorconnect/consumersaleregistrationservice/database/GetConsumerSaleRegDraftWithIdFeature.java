package com.precorconnect.consumersaleregistrationservice.database;

import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import org.checkerframework.checker.nullness.qual.NonNull;

interface GetConsumerSaleRegDraftWithIdFeature {

    ConsumerPartnerSaleRegDraftView execute(
            @NonNull ConsumerSaleRegDraftId partnerSaleRegDraftId
    );

}
