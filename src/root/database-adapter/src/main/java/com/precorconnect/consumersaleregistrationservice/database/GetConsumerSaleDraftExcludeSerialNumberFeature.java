package com.precorconnect.consumersaleregistrationservice.database;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import com.precorconnect.consumersaleregistrationservice.SerialNumber;

public interface GetConsumerSaleDraftExcludeSerialNumberFeature {
	
	ConsumerPartnerSaleRegDraftView execute(
            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
            @NonNull Collection<SerialNumber> serialNumberExclusionList
    );

}
