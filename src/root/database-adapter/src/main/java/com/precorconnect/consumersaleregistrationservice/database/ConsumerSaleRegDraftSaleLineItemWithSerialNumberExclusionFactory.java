package com.precorconnect.consumersaleregistrationservice.database;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.SerialNumber;

public interface ConsumerSaleRegDraftSaleLineItemWithSerialNumberExclusionFactory {
	
	com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItem construct(
            @NonNull ConsumerSaleRegDraftSaleLineItem partnerSaleRegDraftSaleLineItem,
            @NonNull Collection<SerialNumber> serialNumberExclusionList
    );
      
	

}
