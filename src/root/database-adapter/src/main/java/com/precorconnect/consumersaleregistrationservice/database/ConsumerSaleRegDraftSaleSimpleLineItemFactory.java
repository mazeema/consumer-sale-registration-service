package com.precorconnect.consumersaleregistrationservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

interface ConsumerSaleRegDraftSaleSimpleLineItemFactory {

    ConsumerSaleRegDraftSaleSimpleLineItem construct(
            com.precorconnect.consumersaleregistrationservice.
            @NonNull ConsumerSaleRegDraftSaleSimpleLineItem partnerSaleRegDraftSaleSimpleLineItem
    );

    com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem construct(
            com.precorconnect.consumersaleregistrationservice.database.
            @NonNull ConsumerSaleRegDraftSaleSimpleLineItem partnerSaleRegDraftSaleSimpleLineItem
    );

}
