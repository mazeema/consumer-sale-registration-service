package com.precorconnect.consumersaleregistrationservice;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThatThrownBy;

import org.assertj.core.api.StrictAssertions;
import org.junit.Test;

import com.precorconnect.UserId;


public class UpdateSalePartnerRepIdOfConsumerSaleRegDraftReqImplTest {

    /*
    fields
     */
    private final Dummy dummy =
            new Dummy();

    /*
    test methods
     */
    @Test
    public void constructor_NullConsumerSaleRegDraftId_Throws() {

        assertThatThrownBy(() ->
                new UpdateSalePartnerRepIdOfConsumerSaleRegDraftReqImpl(
                        null,
                        dummy.getUserId()
                )
        );

    }

    @Test
    public void constructor_SetsConsumerSaleRegDraftId() {

        /*
        arrange
         */
        ConsumerSaleRegDraftId expectedConsumerSaleRegDraftId =
                dummy.getConsumerSaleRegDraftId();

        /*
        act
         */
        UpdateSalePartnerRepIdOfConsumerSaleRegDraftReq objectUnderTest =
                new UpdateSalePartnerRepIdOfConsumerSaleRegDraftReqImpl(
                        expectedConsumerSaleRegDraftId,
                        dummy.getUserId()
                );

        /*
        assert
         */
        ConsumerSaleRegDraftId actualConsumerSaleRegDraftId =
                objectUnderTest.getPartnerSaleRegDraftId();

        assertThat(actualConsumerSaleRegDraftId)
                .isEqualTo(expectedConsumerSaleRegDraftId);

    }

    @Test
    public void constructor_NullSalePartnerRepId_DoesNotThrow() {

        new UpdateSalePartnerRepIdOfConsumerSaleRegDraftReqImpl(
                dummy.getConsumerSaleRegDraftId(),
                null
        );

    }

    @Test
    public void constructor_SetsSalePartnerRepId() {

        /*
        arrange
         */
        UserId expectedSalePartnerRepId =
                dummy.getUserId();

        /*
        act
         */
        UpdateSalePartnerRepIdOfConsumerSaleRegDraftReq objectUnderTest =
                new UpdateSalePartnerRepIdOfConsumerSaleRegDraftReqImpl(
                        dummy.getConsumerSaleRegDraftId(),
                        expectedSalePartnerRepId
                );

        /*
        assert
         */
        UserId actualSalePartnerRepId =
        		objectUnderTest.getSalePartnerRepId().isPresent()
                ?objectUnderTest.getSalePartnerRepId().get():null;

        StrictAssertions.assertThat(actualSalePartnerRepId)
                .isEqualTo(expectedSalePartnerRepId);

    }

}