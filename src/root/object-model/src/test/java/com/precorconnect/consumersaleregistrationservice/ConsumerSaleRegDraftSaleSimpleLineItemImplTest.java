package com.precorconnect.consumersaleregistrationservice;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThatThrownBy;

import java.math.BigDecimal;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.Ignore;
import org.junit.Test;

import com.precorconnect.AssetId;

public class ConsumerSaleRegDraftSaleSimpleLineItemImplTest {

    /*
    fields
     */
    private final Dummy dummy =
            new Dummy();

    /*
    test methods
     */
    @Ignore
    public void constructor_NullId_Throws() {

        assertThatThrownBy(() ->
                new ConsumerSaleRegDraftSaleSimpleLineItemImpl(
                        null,
                        dummy.getConsumerSaleSimpleLineItem()
                )
        );

    }

    @Test
    public void constructor_SetsId() {

        /*
        arrange
         */
        ConsumerSaleRegDraftSaleLineItemId expectedId =
                dummy.getConsumerSaleRegDraftSaleLineItemId();

        /*
        act
         */
        ConsumerSaleRegDraftSaleSimpleLineItem objectUnderTest =
                new ConsumerSaleRegDraftSaleSimpleLineItemImpl(
                        expectedId,
                        dummy.getConsumerSaleSimpleLineItem()
                );

        /*
        assert
         */
        ConsumerSaleRegDraftSaleLineItemId actualId =
                objectUnderTest.getId();

        assertThat(actualId)
                .isEqualTo(expectedId);

    }

    @Test
    public void constructor_NullConsumerSaleSimpleLineItem_Throws() {

        assertThatThrownBy(() ->
                new ConsumerSaleRegDraftSaleSimpleLineItemImpl(
                        dummy.getConsumerSaleRegDraftSaleLineItemId(),
                        null
                )
        );

    }

    @Test
    public void constructor_SetsPrice() {

        /*
        arrange
         */
        ConsumerSaleSimpleLineItem ConsumerSaleSimpleLineItem =
                dummy.getConsumerSaleSimpleLineItem();

        Optional<BigDecimal> expectedPrice =
                ConsumerSaleSimpleLineItem.getPrice();

        /*
        act
         */
        ConsumerSaleRegDraftSaleSimpleLineItem objectUnderTest =
                new ConsumerSaleRegDraftSaleSimpleLineItemImpl(
                        dummy.getConsumerSaleRegDraftSaleLineItemId(),
                        ConsumerSaleSimpleLineItem
                );

        /*
        assert
         */
        Optional<BigDecimal> actualPrice =
                objectUnderTest.getPrice();

        assertThat(actualPrice)
                .isEqualTo(expectedPrice);

    }

    @Test
    public void constructor_SetsAssetId() {

        /*
        arrange
         */
        ConsumerSaleSimpleLineItem ConsumerSaleSimpleLineItem =
                dummy.getConsumerSaleSimpleLineItem();

        AssetId expectedAssetId =
                ConsumerSaleSimpleLineItem.getAssetId();

        /*
        act
         */
        ConsumerSaleRegDraftSaleSimpleLineItem objectUnderTest =
                new ConsumerSaleRegDraftSaleSimpleLineItemImpl(
                        dummy.getConsumerSaleRegDraftSaleLineItemId(),
                        ConsumerSaleSimpleLineItem
                );

        /*
        assert
         */
        AssetId actualAssetId =
                objectUnderTest.getAssetId();

        Assertions.assertThat(actualAssetId)
                .isEqualTo(expectedAssetId);

    }

}