package com.precorconnect.consumersaleregistrationservice;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThatThrownBy;

import java.time.Instant;

import org.assertj.core.api.StrictAssertions;
import org.junit.Test;

public class UpdateSaleCreatedAtTimestampOfConsumerSaleRegDraftReqImplTest {

    /*
    fields
     */
    private final Dummy dummy =
            new Dummy();

    /*
    test methods
     */
    @Test
    public void constructor_NullConsumerSaleRegDraftId_Throws() {

        assertThatThrownBy(() ->
                new UpdateSaleCreatedAtTimestampOfConsumerSaleRegDraftReqImpl(
                        null,
                        dummy.getInstant()
                )
        );

    }

    @Test
    public void constructor_SetsConsumerSaleRegDraftId() {

        /*
        arrange
         */
        ConsumerSaleRegDraftId expectedConsumerSaleRegDraftId =
                dummy.getConsumerSaleRegDraftId();

        /*
        act
         */
        UpdateSaleCreatedAtTimestampOfConsumerSaleRegDraftReq objectUnderTest =
                new UpdateSaleCreatedAtTimestampOfConsumerSaleRegDraftReqImpl(
                        expectedConsumerSaleRegDraftId, dummy.getInstant()
                );

        /*
        assert
         */
        ConsumerSaleRegDraftId actualConsumerSaleRegDraftId =
                objectUnderTest.getConsumerSaleRegDraftId();

        assertThat(actualConsumerSaleRegDraftId)
                .isEqualTo(expectedConsumerSaleRegDraftId);

    }

    @Test
    public void constructor_NullSaleCreatedAtTimestamp_DoesNotThrow() {

        new UpdateSaleCreatedAtTimestampOfConsumerSaleRegDraftReqImpl(
                dummy.getConsumerSaleRegDraftId(),
                null
        );

    }

    @Test
    public void constructor_SetsSaleCreatedAtTimestamp() {

        /*
        arrange
         */
        Instant expectedSaleCreatedAtTimestamp =
                dummy.getInstant();

        /*
        act
         */
        UpdateSaleCreatedAtTimestampOfConsumerSaleRegDraftReq objectUnderTest =
                new UpdateSaleCreatedAtTimestampOfConsumerSaleRegDraftReqImpl(
                        dummy.getConsumerSaleRegDraftId(),
                        expectedSaleCreatedAtTimestamp
                );

        /*
        assert
         */
        Instant actualSaleCreatedAtTimestamp =
        		objectUnderTest.getSaleCreatedAtTimestamp().isPresent()
                ?objectUnderTest.getSaleCreatedAtTimestamp().get():null;

        StrictAssertions.assertThat(actualSaleCreatedAtTimestamp)
                .isEqualTo(expectedSaleCreatedAtTimestamp);

    }

}