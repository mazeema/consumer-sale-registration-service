package com.precorconnect.consumersaleregistrationservice;

public interface RegistrationId {

	Long getValue();
}
