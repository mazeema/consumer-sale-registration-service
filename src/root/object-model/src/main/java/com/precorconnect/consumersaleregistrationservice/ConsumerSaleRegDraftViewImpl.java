package com.precorconnect.consumersaleregistrationservice;


import java.time.Instant;
import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.AccountId;
import com.precorconnect.FirstName;
import com.precorconnect.LastName;
import com.precorconnect.UserId;

public class ConsumerSaleRegDraftViewImpl
        extends AbstractConsumerSaleRegDraftView
        implements ConsumerPartnerSaleRegDraftView {

    /*
    constructors
     */
    public ConsumerSaleRegDraftViewImpl(
            @NonNull ConsumerSaleRegDraftId id,
            @NonNull FirstName firstName,
	        @NonNull LastName lastName,
	        @NonNull ConsumerPostalAddress address,
	        @Nullable ConsumerPhoneNumber phoneNumber,
	        @Nullable PersonEmail emailAddress,
            @NonNull AccountId partnerAccountId,
            @NonNull Collection<ConsumerSaleRegDraftSaleLineItem> saleLineItems,
            @NonNull Instant sellDate,
            @NonNull IsSubmit isSubmitted,
            @Nullable InvoiceUrl invoiceUrl,
            @NonNull InvoiceNumber invoiceNumber,
            @NonNull Instant saleCreatedAtTimestamp,
            @Nullable UserId salePartnerRepId,
            @NonNull Instant createDate
    ) {

        super(
                id,
                firstName,
                lastName,
                address,
                phoneNumber,
                emailAddress,
                partnerAccountId,
                saleLineItems,
                invoiceNumber,
                saleCreatedAtTimestamp,
                sellDate,
                salePartnerRepId,
                invoiceUrl,
                isSubmitted,
                createDate
        );

      
       
    }

	

}
