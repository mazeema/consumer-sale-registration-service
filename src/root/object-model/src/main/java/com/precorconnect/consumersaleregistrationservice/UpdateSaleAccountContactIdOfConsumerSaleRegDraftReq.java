package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.AccountContactId;

import java.util.Optional;

public interface UpdateSaleAccountContactIdOfConsumerSaleRegDraftReq {

    ConsumerSaleRegDraftId getConsumerSaleRegDraftId();

    Optional<AccountContactId> getSaleAccountContactId();

}
