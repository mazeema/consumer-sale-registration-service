package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.AccountId;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public abstract class AbstractAddConsumerSaleRegDraftReq
        implements AddPartnerSaleRegDraftReq {

    /*
    fields
     */
    private final AccountId partnerAccountId;


    /*
    constructors
     */
    public AbstractAddConsumerSaleRegDraftReq(
            @NonNull AccountId partnerAccountId
    ) {

        this.partnerAccountId =
                guardThat(
                        "partnerAccountId",
                        partnerAccountId
                )
                        .isNotNull()
                        .thenGetValue();
       

    }

    /*
    getter methods
     */
    @Override
    public AccountId getPartnerAccountId() {
        return partnerAccountId;
    }
    /*
    equality methods
     */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((partnerAccountId == null) ? 0 : partnerAccountId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractAddConsumerSaleRegDraftReq other = (AbstractAddConsumerSaleRegDraftReq) obj;
		if (partnerAccountId == null) {
			if (other.partnerAccountId != null)
				return false;
		} else if (!partnerAccountId.equals(other.partnerAccountId))
			return false;
		return true;
	}
   
    
}
