package com.precorconnect.consumersaleregistrationservice;

public interface Amount {
	
	Double getValue();

}
