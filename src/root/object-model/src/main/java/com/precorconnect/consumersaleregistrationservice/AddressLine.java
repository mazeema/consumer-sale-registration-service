package com.precorconnect.consumersaleregistrationservice;

public interface AddressLine {
	
	String getValue();

}
