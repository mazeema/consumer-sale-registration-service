package com.precorconnect.consumersaleregistrationservice;

public interface UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq {

    ConsumerSaleRegDraftId getConsumerSaleRegDraftId();

    InvoiceUrl getInvoiceUrl();

}
