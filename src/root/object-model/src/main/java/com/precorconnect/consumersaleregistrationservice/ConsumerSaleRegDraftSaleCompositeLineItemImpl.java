package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.math.BigDecimal;
import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;


public class ConsumerSaleRegDraftSaleCompositeLineItemImpl
        extends AbstractConsumerSaleRegDraftSaleLineItem
        implements ConsumerSaleRegDraftSaleCompositeLineItem {

    /*
    fields
     */
    private final ConsumerSaleCompositeLineItem consumerSaleCompositeLineItem;

    /*
    constructors
     */
    public ConsumerSaleRegDraftSaleCompositeLineItemImpl(
            @NonNull ConsumerSaleRegDraftSaleLineItemId id,
            @NonNull ConsumerSaleCompositeLineItem consumerSaleCompositeLineItem
    ) {

        super(id);

        this.consumerSaleCompositeLineItem =
                guardThat(
                        "consumerSaleCompositeLineItem",
                        consumerSaleCompositeLineItem
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
     */
    @Override
    public Iterable<ConsumerSaleLineItemComponent> getComponents() {
        return consumerSaleCompositeLineItem.getComponents();
    }

    @Override
    public Optional<BigDecimal> getPrice() {
        return consumerSaleCompositeLineItem.getPrice();
    }
    
    /*
    equality methods
     */


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((consumerSaleCompositeLineItem == null) ? 0 : consumerSaleCompositeLineItem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConsumerSaleRegDraftSaleCompositeLineItemImpl other = (ConsumerSaleRegDraftSaleCompositeLineItemImpl) obj;
		if (consumerSaleCompositeLineItem == null) {
			if (other.consumerSaleCompositeLineItem != null)
				return false;
		} else if (!consumerSaleCompositeLineItem.equals(other.consumerSaleCompositeLineItem))
			return false;
		return true;
	}

}
