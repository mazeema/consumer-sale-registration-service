package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.math.BigDecimal;
import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

public class ConsumerSaleCompositeLineItemImpl implements ConsumerSaleCompositeLineItem {
	
	/*
    fields
     */
    private final Iterable<ConsumerSaleLineItemComponent> components;

    private final BigDecimal price;

    /*
    constructors
     */
    
    public ConsumerSaleCompositeLineItemImpl(
            @NonNull Iterable<ConsumerSaleLineItemComponent> components,
            @Nullable BigDecimal price
    ) {

        this.components =
                guardThat(
                        "components",
                        components
                )
                .isNotNull()
                .hasCountGreaterThan(0L)
                .thenGetValue();

     /*   if (null == price) {

            components.forEach(consumerSaleLineItemComponent -> {
                if (!consumerSaleLineItemComponent.getPrice().isPresent()) {
                    throw new IllegalArgumentException(
                            String.format(
                                    "%s => price must be specified at the component level if it is not specified at the line item level",
                                    getClass().getCanonicalName()
                            )
                    );
                }
            });

        } else {

            components.forEach(consumerSaleLineItemComponent -> {
                if (consumerSaleLineItemComponent.getPrice().isPresent()) {
                    throw new IllegalArgumentException(
                            String.format(
                                    "%s => price can not be specified at the line item component level if it is specified at the line item level",
                                    getClass().getCanonicalName()
                            )
                    );
                }
            });

        }*/

        this.price = price;
    }
    
	@Override
	public Iterable<ConsumerSaleLineItemComponent> getComponents() {
		return components;
	}

	@Override
	public Optional<BigDecimal> getPrice() {
		 return Optional.ofNullable(price);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((components == null) ? 0 : components.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConsumerSaleCompositeLineItemImpl other = (ConsumerSaleCompositeLineItemImpl) obj;
		if (components == null) {
			if (other.components != null)
				return false;
		} else if (!components.equals(other.components))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		return true;
	}
	
}
