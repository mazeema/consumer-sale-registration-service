package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.UserId;

public class UpdateSalePartnerRepIdOfConsumerSaleRegDraftReqImpl
        implements UpdateSalePartnerRepIdOfConsumerSaleRegDraftReq {

    /*
    fields
     */
    private final ConsumerSaleRegDraftId partnerSaleRegDraftId;

    private final UserId salePartnerRepId;

    /*
    constructors
     */
    public UpdateSalePartnerRepIdOfConsumerSaleRegDraftReqImpl(
            @NonNull ConsumerSaleRegDraftId partnerSaleRegDraftId,
            @NonNull UserId salePartnerRepId
    ) {

        this.partnerSaleRegDraftId =
                guardThat(
                        "partnerSaleRegDraftId",
                        partnerSaleRegDraftId
                )
                        .isNotNull()
                        .thenGetValue();

        this.salePartnerRepId = salePartnerRepId;

    }

    /*
    getter methods
     */
    public Optional<UserId> getSalePartnerRepId() {
        return Optional.ofNullable(salePartnerRepId);
    }

    @Override
    public ConsumerSaleRegDraftId getPartnerSaleRegDraftId() {
        return partnerSaleRegDraftId;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UpdateSalePartnerRepIdOfConsumerSaleRegDraftReqImpl that = (UpdateSalePartnerRepIdOfConsumerSaleRegDraftReqImpl) o;

        if (!partnerSaleRegDraftId.equals(that.partnerSaleRegDraftId)) return false;
        return !(salePartnerRepId != null ? !salePartnerRepId.equals(that.salePartnerRepId) : that.salePartnerRepId != null);

    }

    @Override
    public int hashCode() {
        int result = partnerSaleRegDraftId.hashCode();
        result = 31 * result + (salePartnerRepId != null ? salePartnerRepId.hashCode() : 0);
        return result;
    }
}
