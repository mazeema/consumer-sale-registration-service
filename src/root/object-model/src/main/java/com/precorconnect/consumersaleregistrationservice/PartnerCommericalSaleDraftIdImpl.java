package com.precorconnect.consumersaleregistrationservice;


import org.checkerframework.checker.nullness.qual.Nullable;

public class PartnerCommericalSaleDraftIdImpl implements PartnerCommericalSaleDraftId {
	
	 /*
    fields
     */
    private final Long value;

    /*
    constructors
    */
    public PartnerCommericalSaleDraftIdImpl(
            @Nullable Long value
    ) throws IllegalArgumentException {

    	this.value = value;
              

    }
    
    /*
    getter methods
     */

	@Override
	public Long getValue() {
		return value;
	}

	 /*
    equality methods
     */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PartnerCommericalSaleDraftIdImpl other = (PartnerCommericalSaleDraftIdImpl) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	
	

}
