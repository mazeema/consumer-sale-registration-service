package com.precorconnect.consumersaleregistrationservice;

public interface RegistrationReviewId {
	
	Long getValue();

}
