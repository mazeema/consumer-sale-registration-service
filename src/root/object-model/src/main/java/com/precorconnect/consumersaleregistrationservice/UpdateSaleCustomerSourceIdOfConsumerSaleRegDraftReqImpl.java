package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.CustomerSourceId;
import org.checkerframework.checker.nullness.qual.NonNull;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Optional;

public class UpdateSaleCustomerSourceIdOfConsumerSaleRegDraftReqImpl
        implements UpdateSaleCustomerSourceIdOfConsumerSaleRegDraftReq {

    /*
    fields
     */
    private final ConsumerSaleRegDraftId partnerSaleRegDraftId;

    private final CustomerSourceId saleCustomerSourceId;

    /*
    constructors
     */
    public UpdateSaleCustomerSourceIdOfConsumerSaleRegDraftReqImpl(
            @NonNull ConsumerSaleRegDraftId partnerSaleRegDraftId,
            @NonNull CustomerSourceId saleCustomerSourceId
    ) {

    	this.partnerSaleRegDraftId =
                guardThat(
                        "partnerSaleRegDraftId",
                         partnerSaleRegDraftId
                )
                        .isNotNull()
                        .thenGetValue();

        this.saleCustomerSourceId = saleCustomerSourceId;

    }

    /*
    getter methods
     */
    @Override
    public Optional<CustomerSourceId> getSaleCustomerSourceId() {
        return Optional.ofNullable(saleCustomerSourceId);
    }

    @Override
    public ConsumerSaleRegDraftId getPartnerSaleRegDraftId() {
        return partnerSaleRegDraftId;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UpdateSaleCustomerSourceIdOfConsumerSaleRegDraftReqImpl that = (UpdateSaleCustomerSourceIdOfConsumerSaleRegDraftReqImpl) o;

        if (!partnerSaleRegDraftId.equals(that.partnerSaleRegDraftId)) return false;
        return !(saleCustomerSourceId != null ? !saleCustomerSourceId.equals(that.saleCustomerSourceId) : that.saleCustomerSourceId != null);

    }

    @Override
    public int hashCode() {
        int result = partnerSaleRegDraftId.hashCode();
        result = 31 * result + (saleCustomerSourceId != null ? saleCustomerSourceId.hashCode() : 0);
        return result;
    }
}
