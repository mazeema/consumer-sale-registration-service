package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.FirstName;
import com.precorconnect.LastName;
import com.precorconnect.UserId;

public interface UpdateConsumerSaleRegDraftReq extends UpdatePartnerSaleRegDraftReq{
	
	ConsumerSaleRegDraftId getId();
	
	FirstName getFirstName();
	
	LastName getLastName();

	ConsumerPostalAddress getAddress();

    ConsumerPhoneNumber getPhoneNumber();
    
    PersonEmail getPersonEmail();
	
    InvoiceNumber getInvoiceNumber();

    InvoiceUrl getInvoiceUrl();

    IsSubmit getIsSubmitted();

    SellDate getSellDate();

    UserId getUserId();

    Iterable<ConsumerSaleLineItem> getSaleLineItems();
    
    CreateDate getCreateDate();

}
