package com.precorconnect.consumersaleregistrationservice;


public interface PartnerSaleLineItemDto {
	
	SerialNumber getSerialNumber();

}
