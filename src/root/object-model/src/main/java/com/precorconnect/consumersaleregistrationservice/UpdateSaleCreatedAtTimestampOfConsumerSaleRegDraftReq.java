package com.precorconnect.consumersaleregistrationservice;

import java.time.Instant;
import java.util.Optional;

public interface UpdateSaleCreatedAtTimestampOfConsumerSaleRegDraftReq {

    ConsumerSaleRegDraftId getConsumerSaleRegDraftId();

    Optional<Instant> getSaleCreatedAtTimestamp();

}
