package com.precorconnect.consumersaleregistrationservice;

import org.checkerframework.checker.nullness.qual.NonNull;

public class ManagementCompanyIdImpl implements
        ManagementCompanyId {

    /*
    fields
     */
    private final String managementCompanyId;

    /*
    constructors
     */
    public ManagementCompanyIdImpl(
            @NonNull String managementCompanyId
    ) throws IllegalArgumentException {

        this.managementCompanyId = managementCompanyId;

    }

    /*
    getter methods
     */
    @Override
    public String getValue() {
        return managementCompanyId;
    }

    /*
    equality methods
    */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (o == null || getClass() != o.getClass()) {
			return false;
		}

        ManagementCompanyIdImpl that = (ManagementCompanyIdImpl) o;

        return managementCompanyId.equals(that.managementCompanyId);

    }

    @Override
    public int hashCode() {
        return managementCompanyId.hashCode();
    }
}
