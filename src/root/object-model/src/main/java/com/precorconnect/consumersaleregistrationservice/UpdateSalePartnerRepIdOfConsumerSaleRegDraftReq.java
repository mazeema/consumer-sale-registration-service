package com.precorconnect.consumersaleregistrationservice;


import java.util.Optional;

import com.precorconnect.UserId;

public interface UpdateSalePartnerRepIdOfConsumerSaleRegDraftReq {

    ConsumerSaleRegDraftId getPartnerSaleRegDraftId();

    Optional<UserId> getSalePartnerRepId();

}
