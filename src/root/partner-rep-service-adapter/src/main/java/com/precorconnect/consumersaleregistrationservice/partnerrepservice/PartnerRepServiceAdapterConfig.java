package com.precorconnect.consumersaleregistrationservice.partnerrepservice;

import java.net.URL;

public interface PartnerRepServiceAdapterConfig {

    URL getPrecorConnectApiBaseUrl();

}
