package com.precorconnect.consumersaleregistrationservice.partnerrepservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.partnerrepservice.PartnerRepView;

public interface GetPartnerRepsWithIdFeature {
	
	PartnerRepView execute(
            @NonNull UserId id,
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException, AuthorizationException;

}
