package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.OAuth2AccessToken;
import org.checkerframework.checker.nullness.qual.NonNull;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
class RemoveSaleLineItemFromConsumerSaleRegDraftFeatureImpl
        implements RemoveSaleLineItemFromConsumerSaleRegDraftFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public RemoveSaleLineItemFromConsumerSaleRegDraftFeatureImpl(
            @NonNull DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public void execute(
            @NonNull ConsumerSaleRegDraftSaleLineItemId consumerSaleRegDraftSaleLineItemId,
            @NonNull OAuth2AccessToken accessToken
    ) {

        databaseAdapter
                .removeSaleLineItemFromConsumerSaleRegDraft(
                        consumerSaleRegDraftSaleLineItemId
                );

    }
}
