package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.OAuth2AccessToken;
import org.checkerframework.checker.nullness.qual.NonNull;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
class AddSaleLineItemToConsumerSaleRegDraftFeatureImpl
        implements AddSaleLineItemToConsumerSaleRegDraftFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public AddSaleLineItemToConsumerSaleRegDraftFeatureImpl(
            @NonNull DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public ConsumerSaleRegDraftSaleLineItemId execute(
            @NonNull AddSaleLineItemToConsumerSaleRegDraftReq request,
            @NonNull OAuth2AccessToken accessToken
    ) {

        return
                databaseAdapter
                        .addSaleLineItemToConsumerSaleRegDraft(
                                request
                        );

    }
}
