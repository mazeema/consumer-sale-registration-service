package com.precorconnect.consumersaleregistrationservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.PartnerSaleCompositeLineItemWebDto;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.PartnerSaleSimpleLineItemWebDto;

public interface ConsumerSaleLineItemWebDtoFactory {


	PartnerSaleSimpleLineItemWebDto construct(
			com.precorconnect.consumersaleregistrationservice.
			@NonNull ConsumerSaleRegDraftSaleSimpleLineItem consumerSaleRegDraftSaleSimpleLineItem
    );

	PartnerSaleCompositeLineItemWebDto construct(
			com.precorconnect.consumersaleregistrationservice.
			@NonNull ConsumerSaleRegDraftSaleCompositeLineItem consumerSaleRegDraftSaleCompositeLineItem
	    );
}
