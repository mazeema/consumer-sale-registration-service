package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.ProductSaleRegistrationRuleEngineResponseWebView;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.ProductSaleRegistrationRuleEngineWebDto;

@Singleton
public class IsEligibleForSpiffFeatureImpl implements IsEligibleForSpiffFeature {
	
	  private final ProductEligibleSpiffReqFactory productEligibleSpiffReqFactory;
	  
	  private final MasterDataServiceAdapter masterDataServiceAdapter;
	
	
	@Inject
	public IsEligibleForSpiffFeatureImpl(
			 @NonNull ProductEligibleSpiffReqFactory productEligibleSpiffReqFactory,
			 @NonNull MasterDataServiceAdapter masterDataServiceAdapter
			) {
		
		this.productEligibleSpiffReqFactory = 
				 guardThat(
	                        "productEligibleSpiffReqFactory",
	                        productEligibleSpiffReqFactory
	                )
	                        .isNotNull()
	                        .thenGetValue();
		
		this.masterDataServiceAdapter =
                guardThat(
                        "masterDataServiceAdapter",
                        masterDataServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();
	}



	@Override
	public boolean execute(
			@NonNull ProductSaleRegistrationRuleEngineRequestDto productSaleRegistrationRuleEngineRequestDto,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException, AuthorizationException {
		
		boolean flag=false;

		ProductSaleRegistrationRuleEngineWebDto productSaleRegistrationRuleEngineWebDto
									= productEligibleSpiffReqFactory
									   .construct(
											   productSaleRegistrationRuleEngineRequestDto
										);
		
		ProductSaleRegistrationRuleEngineResponseWebView productSaleRegistrationRuleEngineResponseWebView
									= masterDataServiceAdapter
					    				.getSpiffAmountForPartnerCommercialSaleRegistration(
					    						productSaleRegistrationRuleEngineWebDto,
					    						accessToken
					    				);
		
		
		if(null != productSaleRegistrationRuleEngineResponseWebView
				&& productSaleRegistrationRuleEngineResponseWebView.getAmount() > 0)
			flag=true;
			
		return flag;
	}

}
