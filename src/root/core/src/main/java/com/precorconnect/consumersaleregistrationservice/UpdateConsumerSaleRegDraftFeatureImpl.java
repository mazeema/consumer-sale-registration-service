package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.OAuth2AccessToken;

@Singleton
public class UpdateConsumerSaleRegDraftFeatureImpl implements UpdateConsumerSaleRegDraftFeature {
	
	/*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public UpdateConsumerSaleRegDraftFeatureImpl(
            @NonNull DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public ConsumerPartnerSaleRegDraftView execute(
			@NonNull UpdateConsumerSaleRegDraftReq request,
			@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
			@NonNull OAuth2AccessToken accessToken
			) {
		
		return
                databaseAdapter
                        .updateConsumerSaleRegDraft(
                                request,
                                consumerSaleRegDraftId
                        );
	}

}
