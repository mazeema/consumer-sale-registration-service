package com.precorconnect.consumersaleregistrationservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.claimspiffservice.objectmodel.SpiffAmount;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;

public interface CreateSpiffEntitlementsRequestFactory {

	SpiffEntitlementDto construct(
    		  @NonNull ConsumerPartnerSaleRegDraftView consumerCommercialSaleRegDraftView,
    		  @NonNull SpiffAmount spiffAmount
    );

}
