package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.OAuth2AccessToken;

@Singleton
class AddConsumerSaleRegDraftFeatureImpl
        implements AddConsumerSaleRegDraftFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public AddConsumerSaleRegDraftFeatureImpl(
            @NonNull DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public ConsumerSaleRegDraftId execute(
            @NonNull AddConsumerSaleRegDraftReq request,
            @NonNull OAuth2AccessToken accessToken
    ) {

        return
                databaseAdapter
                        .addConsumerSaleRegDraft(
                                request
                        );

    }
}
