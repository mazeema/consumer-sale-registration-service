package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.*;
import org.checkerframework.checker.nullness.qual.NonNull;

public interface IdentityServiceAdapter {

    AccessContext getAccessContext(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException;

    PartnerRepAccessContext getPartnerRepAccessContext(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException;

    AppAccessContext getAppAccessContext(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException;
    
    EmployeeAccessContext getEmployeeAccessContext(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException;

}
