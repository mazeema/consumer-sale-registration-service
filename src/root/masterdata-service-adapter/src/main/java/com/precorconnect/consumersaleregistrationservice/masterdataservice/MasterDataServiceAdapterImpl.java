package com.precorconnect.consumersaleregistrationservice.masterdataservice;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.consumersaleregistrationservice.MasterDataServiceAdapter;
import com.precorconnect.consumersaleregistrationservice.SpiffSerialExclusionDto;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.ProductSaleRegistrationRuleEngineResponseWebView;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.ProductSaleRegistrationRuleEngineWebDto;

@Singleton
public class MasterDataServiceAdapterImpl implements
			MasterDataServiceAdapter {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    public MasterDataServiceAdapterImpl(
            @NonNull MasterDataServiceAdapterConfig config
    ) {

        GuiceModule guiceModule =
                new GuiceModule(config);

        injector =
                Guice.createInjector(guiceModule);
    }

    @Override
    public ProductSaleRegistrationRuleEngineResponseWebView getSpiffAmountForPartnerCommercialSaleRegistration(
    		@NonNull ProductSaleRegistrationRuleEngineWebDto productSaleRegistrationRuleEngineWebDto,
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException {

        return
                injector
                        .getInstance(GetSpiffAmountForPartnerCommercialSaleRegistrationFeature.class)
                        .execute(productSaleRegistrationRuleEngineWebDto,accessToken);

    }

	@Override
	public Collection<SpiffSerialExclusionDto> listSpiffSerialExclusion(@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException {
		
		return
				
			injector
	                .getInstance(ListSpiffSerailExclusionFeature.class)
	                .execute(accessToken);

	}


}
