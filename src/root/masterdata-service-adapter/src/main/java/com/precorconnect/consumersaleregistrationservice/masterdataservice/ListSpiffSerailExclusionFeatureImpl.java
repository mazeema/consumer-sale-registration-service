package com.precorconnect.consumersaleregistrationservice.masterdataservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.stream.Collectors;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.consumersaleregistrationservice.SpiffSerialExclusionDto;
import com.precorconnect.spiffmasterdataservice.sdk.SpiffRuleEngineServiceSdk;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.SpiffSerialExclusionWebView;

@Singleton
public class ListSpiffSerailExclusionFeatureImpl implements ListSpiffSerailExclusionFeature {

	/*
    fields
     */
    private final SpiffRuleEngineServiceSdk spiffRuleEngineServiceSdk;
    
    private final SpiffSerialExclusionResponseFactory spiffSerialExclusionResponseFactory;

    /*
    constructors
     */
    @Inject
    public ListSpiffSerailExclusionFeatureImpl(
            @NonNull SpiffRuleEngineServiceSdk spiffRuleEngineServiceSdk,
            @NonNull SpiffSerialExclusionResponseFactory spiffSerialExclusionResponseFactory
    ) {

    	this.spiffRuleEngineServiceSdk =
                guardThat(
                        "spiffRuleEngineServiceSdk",
                        spiffRuleEngineServiceSdk
                )
                        .isNotNull()
                        .thenGetValue();
    	
    	this.spiffSerialExclusionResponseFactory =
                guardThat(
                        "spiffSerialExclusionResponseFactory",
                        spiffSerialExclusionResponseFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public Collection<SpiffSerialExclusionDto> execute(@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException {
		
		 
		Collection<SpiffSerialExclusionWebView> spiffSerialExclusionList= spiffRuleEngineServiceSdk
																							.listSpiffSerailExclusion(
																										accessToken
																							);
		return 
				spiffSerialExclusionList
				.stream()
				.map(spiffSerialExclusionResponseFactory::construct)
				.collect(Collectors.toList());
	}

}
