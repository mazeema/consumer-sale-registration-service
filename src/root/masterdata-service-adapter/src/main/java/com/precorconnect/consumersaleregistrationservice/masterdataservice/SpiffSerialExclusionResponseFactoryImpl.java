package com.precorconnect.consumersaleregistrationservice.masterdataservice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.Description;
import com.precorconnect.consumersaleregistrationservice.DescriptionImpl;
import com.precorconnect.consumersaleregistrationservice.SerialNumber;
import com.precorconnect.consumersaleregistrationservice.SerialNumberImpl;
import com.precorconnect.consumersaleregistrationservice.SpiffSerialExclusionDto;
import com.precorconnect.consumersaleregistrationservice.SpiffSerialExclusionDtoImpl;
import com.precorconnect.consumersaleregistrationservice.SpiffSerialExclusionId;
import com.precorconnect.consumersaleregistrationservice.SpiffSerialExclusionIdImpl;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.SpiffSerialExclusionWebView;

@Singleton
public class SpiffSerialExclusionResponseFactoryImpl implements SpiffSerialExclusionResponseFactory {

	@Override
	public SpiffSerialExclusionDto construct(
			@NonNull SpiffSerialExclusionWebView spiffSerialExclusion
			) {
		try {
			
		SpiffSerialExclusionId spiffSerialExclusionId=
				new SpiffSerialExclusionIdImpl(
							spiffSerialExclusion.getId()
						);
		
		SerialNumber serialNumber=
				new SerialNumberImpl(
						spiffSerialExclusion.getSerialNumber()
						);
		Description description=
				new DescriptionImpl(
						spiffSerialExclusion.getDescription()
						);
		
		Instant dateCreate;
	
			dateCreate = new SimpleDateFormat("MM/dd/yyyy")
			.parse(spiffSerialExclusion.getDateCreate())
			.toInstant();
		
		return 
				new SpiffSerialExclusionDtoImpl(
						spiffSerialExclusionId,
						serialNumber,
						description,
						dateCreate
			);
		
		} catch (ParseException e) {
			e.printStackTrace();
			 throw new RuntimeException(e);
		}
	}

}
