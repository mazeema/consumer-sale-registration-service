package com.precorconnect.partnersaleregdraftservice.registrationlogservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.registrationlogservice.sdk.RegistrationLogServiceSdk;
import com.precorconnect.registrationlogservice.webapi.AddRegistrationLog;

@Singleton
public class AddRegistrationLogFeatureImpl 
				implements AddRegistrationLogFeature{

	   /*
    fields
     */
    private final RegistrationLogServiceSdk registrationLogServiceSdk;

    /*
    constructors
     */
    @Inject
    public AddRegistrationLogFeatureImpl(
            @NonNull RegistrationLogServiceSdk registrationLogServiceSdk
    ) {

    	this.registrationLogServiceSdk =
                guardThat(
                        "registrationLogServiceSdk",
                        registrationLogServiceSdk
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
	public Long addRegistrationLog(
			@NonNull AddRegistrationLog request,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException  {

        return 
        		registrationLogServiceSdk
                	.addRegistrationLog(
                			request,
                			accessToken
                			);

    }
}


