package com.precorconnect.consumersaleregistrationservice.updatesaleinvoiceurlofconsumersaleregdraftfeature;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.OAuth2AccessTokenImpl;
import com.precorconnect.consumersaleregistrationservice.AccessTokenFactory;
import com.precorconnect.consumersaleregistrationservice.Config;
import com.precorconnect.consumersaleregistrationservice.ConfigFactory;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftIdImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegistrationService;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegistrationServiceImpl;
import com.precorconnect.consumersaleregistrationservice.Dummy;
import com.precorconnect.consumersaleregistrationservice.InvoiceUrl;
import com.precorconnect.consumersaleregistrationservice.UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq;
import com.precorconnect.consumersaleregistrationservice.UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReqImpl;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefs {
	
	
	private final Config config = new ConfigFactory().construct();

	private final Dummy dummy = new Dummy();

	private final AccessTokenFactory factory =
	           new AccessTokenFactory(
	           		dummy,
	                   new IdentityServiceIntegrationTestSdkImpl(
	                   			config
	                                     .getIdentityServiceJwtSigningKey()
	                   )
	           );
	
	private OAuth2AccessToken accessToken;
	
	private UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq request;
	
	private ConsumerSaleRegDraftId consumerSaleRegDraftId;
	
	private InvoiceUrl invoiceUrl;
	
	@Given("^an updateSaleInvoiceUrlOfConsumerSaleRegDraft has attributes$")
	public void an_updateSaleInvoiceUrlOfConsumerSaleRegDraft_has_attributes(DataTable arg1) throws Throwable {
	   
	}

	@Given("^consumerSaleRegDraftId is the id of an existing consumerSaleRegDraftId$")
	public void consumersaleregdraftid_is_the_id_of_an_existing_consumerSaleRegDraftId() throws Throwable {
	 
		consumerSaleRegDraftId = new ConsumerSaleRegDraftIdImpl(1000090L);
	}

	@Given("^I provide an invoiceUrl to existing consumerSaleRegDraftId$")
	public void i_provide_an_invoiceUrl_to_existing_consumerSaleRegDraftId() throws Throwable {
		
		invoiceUrl = dummy.getInvoiceUrl();
	 
	}

	@Given("^invoiceUrl is represented in the Database by dbUpdateSaleInvoiceUrlOfConsumerSaleRegDraft\\.invoiceURL$")
	public void invoiceurl_is_represented_in_the_Database_by_dbUpdateSaleInvoiceUrlOfConsumerSaleRegDraft_invoiceURL() throws Throwable {
	    
	}

	@Given("^I provide an accessToken identifying me as a consumer rep associated with consumerSaleRegDraftId\\.partnerAccountId$")
	public void i_provide_an_accessToken_identifying_me_as_a_consumer_rep_associated_with_consumerSaleRegDraftId_partnerAccountId() throws Throwable {
		
		accessToken = new OAuth2AccessTokenImpl(
				factory
					.constructValidPartnerRepOAuth2AccessToken(dummy.getAccountId())
					.getValue()
				);
	}

	@When("^I execute updateSaleInvoiceUrlOfConsumerSaleRegDraft$")
	public void i_execute_updateSaleInvoiceUrlOfConsumerSaleRegDraft() throws Throwable {
		
		ConsumerSaleRegistrationService objectUnderTest = new ConsumerSaleRegistrationServiceImpl(config.getConsumerSaleRegistrationServiceConfig());

		request = new UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReqImpl(consumerSaleRegDraftId,
				invoiceUrl);
		
		objectUnderTest.updateSaleInvoiceUrlOfConsumerSaleRegDraft(request, accessToken);
	   
	}

	@Then("^dbUpdateSaleInvoiceUrlOfConsumerSaleRegDraft\\.invoiceUrl is updated to updateSaleInvoiceUrlOfConsumerSaleRegDraft\\.invoiceURL$")
	public void dbupdatesaleinvoiceurlofconsumersaleregdraft_invoiceUrl_is_updated_to_updateSaleInvoiceUrlOfConsumerSaleRegDraft_invoiceURL() throws Throwable {
	   
	}



}
