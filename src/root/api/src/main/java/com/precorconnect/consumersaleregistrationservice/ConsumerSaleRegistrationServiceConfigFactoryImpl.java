package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.consumersaleregistrationservice.claimspiffservice.ClaimSpiffServiceAdapterConfigFactoryImpl;
import com.precorconnect.consumersaleregistrationservice.database.DatabaseAdapterConfigFactoryImpl;
import com.precorconnect.consumersaleregistrationservice.identityservice.IdentityServiceAdapterConfigFactoryImpl;
import com.precorconnect.consumersaleregistrationservice.masterdataservice.MasterDataServiceAdapterConfigFactoryImpl;
import com.precorconnect.consumersaleregistrationservice.partnerrepservice.PartnerRepServiceAdapterConfigFactoryImpl;
import com.precorconnect.partnersaleregdraftservice.registrationlogservice.RegistrationLogServiceAdapterConfigFactoryImpl;

public class ConsumerSaleRegistrationServiceConfigFactoryImpl
        implements ConsumerSaleRegistrationServiceConfigFactory {

    @Override
    public ConsumerSaleRegistrationServiceConfig construct() {

        return
                new ConsumerSaleRegistrationServiceConfigImpl(
                        new IdentityServiceAdapterConfigFactoryImpl()
                                .construct(),
                        new DatabaseAdapterConfigFactoryImpl()
                                .construct(),
                        new ClaimSpiffServiceAdapterConfigFactoryImpl()
                        		.construct(),
                		new MasterDataServiceAdapterConfigFactoryImpl()
                				.construct(),
                		new RegistrationLogServiceAdapterConfigFactoryImpl()
                				.construct(),
                		new PartnerRepServiceAdapterConfigFactoryImpl()
                				.construct()
                );

    }
}
