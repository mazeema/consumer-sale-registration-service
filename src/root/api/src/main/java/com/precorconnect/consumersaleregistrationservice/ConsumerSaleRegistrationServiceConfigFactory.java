package com.precorconnect.consumersaleregistrationservice;

public interface ConsumerSaleRegistrationServiceConfigFactory {

	ConsumerSaleRegistrationServiceConfig construct();   
}
