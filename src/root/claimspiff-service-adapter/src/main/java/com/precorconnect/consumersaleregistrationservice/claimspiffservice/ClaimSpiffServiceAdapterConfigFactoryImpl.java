package com.precorconnect.consumersaleregistrationservice.claimspiffservice;

import java.net.MalformedURLException;
import java.net.URL;

public class ClaimSpiffServiceAdapterConfigFactoryImpl
        implements ClaimSpiffServiceAdapterConfigFactory {

    @Override
    public ClaimSpiffServiceAdapterConfig construct() {

        return new ClaimSpiffServiceAdapterConfigImpl(
                constructPrecorConnectApiBaseUrl()
        );

    }

    private URL constructPrecorConnectApiBaseUrl() {

        String precorConnectApiBaseUrl = System.getenv("PRECOR_CONNECT_API_BASE_URL");

        try {

            return new URL(precorConnectApiBaseUrl);

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }

    }
}
