package com.precorconnect.consumersaleregistrationservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import com.precorconnect.consumersaleregistrationservice.webapi.ConsumerSaleRegSynopsisView;

@Singleton
public class ListConsumerSaleRegDraftWithIdsFeatureImpl 
		implements ListConsumerSaleRegDraftWithIdsFeature {
	
	 /*
    fields
     */
    private final WebTarget consumerSaleRegDraftWebTarget;
    
    /*
    constructors
     */
    @Inject
    public ListConsumerSaleRegDraftWithIdsFeatureImpl(
    		@NonNull final WebTarget consumerSaleRegDraftWebTarget
    ) {

    	this.consumerSaleRegDraftWebTarget =
                guardThat(
                        "consumerSaleRegDraftWebTarget",
                        consumerSaleRegDraftWebTarget
                )
                        .isNotNull()
                        .thenGetValue().path("/consumer-sale-registration");
    	
    }

	@Override
	public Collection<ConsumerSaleRegSynopsisView> execute(
			Collection<ConsumerSaleRegDraftId> consumerSaleRegDraftIds,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException, AuthorizationException {
		
		String authorizationHeaderValue =
                String.format(
                        "Bearer %s",
                        accessToken.getValue()
                );
		
		Collection<Long> draftIdsList = consumerSaleRegDraftIds
									  .stream()
									  .map(draftId->draftId.getValue())
									  .collect(Collectors.toList());
		
        Collection<ConsumerSaleRegSynopsisView> consumerCommercialSaleRegSynopsisViews;
        try {
        	consumerCommercialSaleRegSynopsisViews =
        			consumerSaleRegDraftWebTarget
        					.path("/drafts")
                            .request(MediaType.APPLICATION_JSON_TYPE)
                            .header("Authorization", authorizationHeaderValue)
                            .post(
                            		Entity.entity(
                            				draftIdsList,
                                            MediaType.APPLICATION_JSON_TYPE
                                    ),
                                    new GenericType<
                                            Collection<ConsumerSaleRegSynopsisView>>() {
                                    }
                            );

        } catch (Exception e) {

            throw new AuthenticationException(e);

        }

        return
        		consumerCommercialSaleRegSynopsisViews;
                
	}

}
