package com.precorconnect.consumersaleregistrationservice.sdk;

import java.util.Collection;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import com.precorconnect.consumersaleregistrationservice.webapi.ConsumerSaleRegSynopsisView;

public class ConsumerSaleRegistrationServiceSdkImpl 
				implements ConsumerSaleRegistrationServiceSdk {
	
	/*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    @Inject
    public ConsumerSaleRegistrationServiceSdkImpl(
    		@NonNull ConsumerSaleRegistrationServiceConfig config
    		) {
		
    	GuiceModule guiceModule=
						new GuiceModule(config);
		
		this.injector = Guice.createInjector(guiceModule);
	}
    

	@Override
	public ConsumerSaleRegSynopsisView getConsumerSaleRegistrationDraftWithDraftId(
			@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException {
	
		return 
				injector.getInstance(GetConsumerSaleRegDraftWithIdFeature.class)
				.execute(consumerSaleRegDraftId, accessToken);
	}


	@Override
	public Collection<ConsumerSaleRegSynopsisView> listConsumerSaleRegDraftsWithIds(
			@NonNull Collection<ConsumerSaleRegDraftId> consumerSaleRegDraftIds, 
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException, AuthorizationException {
			
		return 
				injector.getInstance(ListConsumerSaleRegDraftWithIdsFeature.class)
				.execute(consumerSaleRegDraftIds, accessToken);
	}


}
