/*package com.precorconnect.consumersaleregistrationservice.sdk;

import static org.assertj.core.api.StrictAssertions.assertThat;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftIdImpl;
import com.precorconnect.consumersaleregistrationservice.webapi.ConsumerSaleRegSynopsisView;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = com.precorconnect.consumersaleregistrationservice.webapi.Application.class)
@WebIntegrationTest({"server.port=0"})
public class ConsumerSaleRegistrationServiceSdkIT {
	
	
    
      fields
      
     
    @Value("${local.server.port}")
    int port;

    private final Dummy dummy =
    		 new Dummy();

    private final Config config =
            new ConfigFactory()
                    .construct();

    private final Factory factory =

    		new Factory(
                    new IdentityServiceIntegrationTestSdkImpl(
                    		config.getIdentityServiceJwtSigningKey()
                    ),
                    dummy
            );
    
    
    
      test methods
      
    @Test
    public void GetPartnerCommercialSaleRegDraft_WithAccountIdTest(
    ) throws Exception {

    	ConsumerSaleRegistrationServiceSdk objectUnderTest =
                new ConsumerSaleRegistrationServiceSdkImpl(
                        new ConsumerSaleRegistrationServiceConfigFactoryImpl()
                        										.construct()
                );

    	ConsumerSaleRegSynopsisView
        			partnerCommercialSaleRegDraftViews =
        			objectUnderTest
                		.getConsumerSaleRegistrationDraftWithDraftId(
                				dummy.getConsumerSaleRegDraftId(),
                				factory.constructValidPartnerRepOAuth2AccessToken()
                				);

        assertThat(partnerCommercialSaleRegDraftViews)
                .isEqualTo(partnerCommercialSaleRegDraftViews);

    }
    
    @Test
    public void listConsumerSaleRegDraftsWithIds_WithDraftIds(
    ) throws Exception {

    	ConsumerSaleRegistrationServiceSdk objectUnderTest =
                new ConsumerSaleRegistrationServiceSdkImpl(
                        new ConsumerSaleRegistrationServiceConfigFactoryImpl()
                        										.construct()
                );

		Collection<ConsumerSaleRegDraftId> consumerSaleRegDraftIds = 
				new ArrayList<ConsumerSaleRegDraftId>();
	    	
		ConsumerSaleRegDraftId firstDraftId	= new ConsumerSaleRegDraftIdImpl(1000455L);
		ConsumerSaleRegDraftId secondDraftId = new ConsumerSaleRegDraftIdImpl(1000456L);
		consumerSaleRegDraftIds.add(firstDraftId);
		consumerSaleRegDraftIds.add(secondDraftId);
	
	        Collection<ConsumerSaleRegSynopsisView>
	        		consumerSaleRegDraftViews =
	                	objectUnderTest.
	                	listConsumerSaleRegDraftsWithIds(
	                			consumerSaleRegDraftIds,
	                			factory.constructValidPartnerRepOAuth2AccessToken()
	                			);
	                
	        assertThat(consumerSaleRegDraftViews.size())
	                .isGreaterThan(0);
	
	    }
    
}
*/